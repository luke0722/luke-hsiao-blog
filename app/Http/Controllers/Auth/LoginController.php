<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function apiLogin(Request $request) {

        $email = $request->input('email');
        $pwd = $request->input('password');
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(
                [
                    'success' => 0,
                    'error_msg' => $validator->errors()
                ]
            );
        }
        $user = User::where('email', '=', $email)->first();

        if($user != null && Hash::check($pwd, $user->password)) {
            if($user->api_token == null) {
                $user->api_token = Str::random(80);
                $user->save();
            }
            return response()->json(
                [
                    'success' => 1,
                    'user' => $user
                ]
            );
        } else {
            return response()->json(
                [
                    'success' => 0,
                    'error_msg' => 'login failed.'
                ]
            );
        }


    }
}
