<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/login', 'Auth\LoginController@apiLogin')->name('api-login');

Route::post('/register', 'Auth\RegisterController@apiRegister')->name('api-register');

Route::prefix('blogs')->group(function () {
    
    Route::get('/list', 'BlogController@apiList')->name('api-blogs-list');
    Route::post('/store', 'BlogController@apiStore')->name('api-blogs-store');
    Route::post('/update', 'BlogController@apiUpdate')->name('api-blogs-update');
    Route::post('/delete', 'BlogController@apiDelete')->name('api-blogs-delete');
    Route::get('/detail/{id}', 'BlogController@apiDetail')->name('api-blogs-detail');


});


